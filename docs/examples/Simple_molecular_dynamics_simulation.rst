.. _Simple molecular dynamics simulation:

Simple molecular dynamics simulation
====================================

**Almost the simplest possible simulation**

A simple simulation illustrating how to set up a small block of atoms, give one of the atoms a "kick", and then run molecular dynamics printing the energy per atom of the system for every ten timesteps.

Copy the script :git:`~docs/examples/SimpleMD.py` to your computer, and try it out.  Experiment!

.. literalinclude:: SimpleMD.py

