.. _Running Polyhedral Template Matching:

====================================
Running Polyhedral Template Matching
====================================

This is almost the same script as the previous example (:ref:`Running Common Neighbor Analysis`), but using the new Polyhedral Template
Matching algorithm instead.  If you run both scripts you will notice
that PTM sees an abrupt crystallization of the cluster at a temperature
where CNA still is not able to distiguish much structure in the
cluster.

The final structure below looks very different from the one displayed
with the previous example.  This is because the two simulations
produced different final clusters; the Langevin dynamics is not
deterministic.  At 300K the performance of CNA and PTM are very similar. 


.. figure:: ../_images/ptmfinal.png
   :figwidth: 350

   A cut through the cluster. FCC atoms are white, HCP atoms red and
   all other atoms are blue.


:git:`~docs/examples/PTMdemo.py`

.. literalinclude:: PTMdemo.py

