.. _Bugs:

====
Bugs
====

If you think Asap contains a bug, you the best course of action is to
report it on the `ASE mailing list`_, or on the GitLab `bug tracking system`_.

* If you are unsure if it is a real bug, or a usage problem, it is
  probably best to report the problem on the `ASE mailing list`_.

  Please provide the failing script as well as the information about
  your environment (processor architecture, versions of python and
  numpy). Then we (or other users) can help you to find out if it is a
  bug.

  Another advantage of reporting bugs on the mailing list: often other
  users will tell you how to work around the bug (until it is solved).

* If you think it is a bug, you can also report it directly on our
  `bug tracking system`_ og GitLab.  This way, it will not be
  forgotten (which may be a risk on the mailing list)

* If you think the bug is in ASE rather than Asap, please see the `ASE
  bug page`_.  If in doubt, report it here, the bug can always be
  transferred - and it goes to the same people anyway.


We need your bug report!
========================

It is essential for maintaining the quality and robustness of the code
that we get bug reports.  **Contribute to ASE and ASAP: report the bugs
you find!**


.. _ASE mailing list: https://wiki.fysik.dtu.dk/ase/contact.html
.. _bug tracking system: https://gitlab.com/asap/asap/issues
.. _ASE bug page: https://wiki.fysik.dtu.dk/ase/development/bugs.html
