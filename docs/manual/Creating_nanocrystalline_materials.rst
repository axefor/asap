.. _creating nanocrystalline materials:

==================================
Creating nanocrystalline materials
==================================

The module ``asap3.setup.nanocrystalline`` contains a number of
functions that can help setting up a nanocrystalline material.

``make_nanocrystal(size, centres, rotations, min_dist=2.0, unit=None, symbol=None, latticeconstant=None)``:
  Create a nanocrystalline sample.

  Parameters:

    ``size``:      Size of the system, in Angstrom (either a number or three numbers).

    ``centres``:   Positions of grains in scaled coordinates
    (i.e. between 0.0 and 1.0).
    
    ``rotations``: Rotation matrices for the grains.

    ``min_dist``: (optional) If two atoms are closer than this
    distance, one of them is removed (default: 2.0).

    ``unit``: (optional) A unit cell for building the crystals (Atoms
    object).  MUST be orthorhombic!
	       
    ``symbol``: (optional) If unit is None, then an FCC crystal of
    this element is used.
	       
    ``latticeconstant`` (optional): If symbol is specified, this
    overrides the default lattice constant.

  Either ``unit`` or ``symbol`` must be specified.
  
There are also a number of "helper functions" that can be used to
help creating the material, either by generating some of the arrays
that ``make_nanocrystal`` needs as input, or by post-processing the
resulting Atoms object.

``bcc_grains(size, perturbation=0.0)``:
  Creates a grain layout based on a randomly perturbed BCC lattice.  The
  ``size`` argument is a 3-tuple giving the number of BCC unit cells

  If ``perturbation`` is non-zero, the position of each grain is
  randomly perturbed by an amount drawn from a normal distribution
  with this standard deviation.

  The function returns two numpy array, the first is the grain
  centers, the second a corresponding number of random rotation
  matrices.  These arrays can be passed directly as the ``centres``
  and ``rotations`` arguments of the ``make_nanocrystal`` function.

``minimize_energy(atoms, nstep, pressure_interval=10, bulkmodulus = 140e9/1e5)``:
    Helper function for minimizing the energy of a nanocrystalline structure.

    It minimizes the energy while eliminating the diagonal components of the stress.
    The atomic positions are optimized using the FIRE algorithm and the stress
    is minimized using the Inhomogeneous_Berendsen algorithm.

    Parameters:

    ``atoms``: The atoms object to be energy-minimized.

    ``nsteps``: Number of time steps in the FIRE algorithm

    ``pressure_interval``: (optional, default is 10) How often to call the stress minimizer.

    ``bulkmodulus``: (optional, default is 1.4e6).  The bulk modulus (in bar !!!) used by the
    stress optimizer.  This value is for Cu and is useful for all metals.
    The value is uncritical, but the order of magnitude should be reasonable. 
