.. _Molecular dynamics with Asap and ASE:

====================================
Molecular dynamics with Asap and ASE
====================================

Typical computer simulations involve moving the atoms around, either
to optimize a structure (energy minimization) or to do molecular
dynamics.  The `Atomic Simulation Environment (ASE)`_ defines a
number of object doing just that.  Documentation of these methods can
be found on the page called `Molecular Dynamics`_ in the `ASE online documentation`_.


Using ASE Molecular Dynamics objects with Asap
==============================================

The standard ASE molecular dynamics objects can directly be used with
Asap simulations, except if the Asap simulation is on a parallel
computer, where some of the objects will fail.  To handle this
situation, special versions of most of the ASE dynamics objects are
provided, which are modified to handle both serial and parallel
simulations.  **We recommend always to use the Asap-provided dynamics
objects, also for serial simulations.**  In that way you will not forget
to modify your script when switching to parallel simulations, and you
will benefit from any Asap-specific optimizations.

The Asap molecular dynamics objects are used exactly as the ASE
version, as documented on the ASE `Molecular Dynamics`_ page.  The
only difference is that you should import them from ``asap3.md``
instead of from ``ase.md``, i.e. replace
::

    from ase.md.verlet import VelocityVerlet

with
::

    from asap.md.verlet import VelocityVerlet
   

.. _`Atomic Simulation Environment (ASE)`: https://wiki.fysik.dtu.dk/ase
.. _`Molecular Dynamics`: https://wiki.fysik.dtu.dk/ase/ase/md.html
.. _`ASE online documentation`: https://wiki.fysik.dtu.dk/ase/ase/ase.html
