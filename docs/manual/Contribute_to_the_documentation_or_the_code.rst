.. _Contribute to the documentation or the code:

Contribute to the documentation or the code
===========================================


Please report bugs!
-------------------

One of the most valuable contributions you can do to the quality of
this code is to :ref:`report bugs <Bugs>` instead of just working around it.

Please report bugs on the :ref:`ASE mailing list <ase:contact>` or on
GitLab_.

.. _GitLab: https://gitlab.com/asap/asap/-/issues

Contribute to this documentation
--------------------------------

If you find errors or omissions on these pages, or if you had a hard
time figuring something out because the pages were unclear, then
*please contribute to the documentation*.

* If you are willing to edit the Restructured Text source code of the
  documentation (in the ``doc`` folder of the ASAP source code),
  please feel free to submit a merge request with the improved
  documentation.

* You are also welcome to open an issue on our bug tracker, or write
  on our mailing list and describe what you think should be chaged.
  Optimally, include the new wording so we can cut-and-paste it into
  the documentation.

Please do not hesitate to do this, we really want your contributions!

Contribute to the Asap code
---------------------------

If you want to improve the Asap code you should first be sure that you
are using the latest version from GitLab.  If you just have minor
changes, then either mail them to `Jakob <JakobSchiøtz>`_ or better
yet upload your patch to GitLab

If you really start working on the code, you should probably fork the
project on GitLab.

