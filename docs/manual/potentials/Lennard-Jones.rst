.. _Lennard-Jones:

===========================
The Lennard-Jones potential
===========================

The good old classic Lennard-Jones potential.  Be aware that it does
not describe real systems very well, except for the famous material
Lennardjonesium, widely described in the scientific literature. :-)

The Lennard-Jones calculator, ``LJPotential``, takes a number of
parameters

::

    LennardJones(elements, epsilon, sigma, rCut=-1, modified=True, 
                 verbose=None)

**elements**:  A list of these elements (their atomic numbers).
You do not need to use "real" atomic numbers, 0,1,2,... will do
just nicely if you do not model real elements.

**epsilon** and **sigma**:  The Lennard-Jones parameters.  These
should be given as NxN symmetric or lower triangular matrices if there are N
elements.  The diagonal elements are for same-element
interactions, the off-diagonal ones are for interactions between
elements.

**rCut**:  The cutoff length.  Default: 3 times the largest value
of sigma.

**modified**: If true, a constant is subtracted from the potential
energy, so it is zero at the cutoff.


Nano-FAQ
========

Q: What are good Lennard-Jones parameters for copper?

A: There are no good Lennard-Jones parameters for copper!  If you want to model a real material, do not use a toy potential.
