.. _Monte Carlo simulations:

=======================
Monte Carlo simulations
=======================

It is obviously possible to run Metropolis Monte Carlo simulations
with Asap, since all that is required is the energies of the atoms.

However, the performance is not necessarily good, since Asap will
normally recalculate the energies of all atoms even if just one atom
has moves.  In many Monte Carlo simulations only one or a few atoms
move in each step.

The data structures of Asap were designed - and the potentials were
optimized - with molecular dynamics in mind, not Monte Carlo.
However, a special version of the :ref:`EMT` potential is available, called
``MonteCarloEMT`` which attempts to address that.  It is used exactly
like the normal EMT potential, but keeps track of which atoms have
moved, and only recalculates what is necessary.

The performance is, however, not impressive, due to problems with the
data structures in the neighbor list.  It may improve in later
versions of Asap, if the need arises.
