.. _creating dislocations:

=====================
Creating dislocations
=====================


**Quick guide:**  See the example :ref:`Creating a dislocation in a slab`.

There are three functions for creating dislocations in a material:
``Dislocation``, `ScrewDislocation`` and ``EdgeDislocation`` for
setting up a general dislocation, a screw dislocation and an edge
dislocation.  It is perfectly allright to use the general
``Dislocation`` function to set up and edge or screw dislocation.


``Dislocation(origin, line, Burgers, PoissonRatio=None, debug=None)``:
  Set up a dislocation with mixed screw and edge character.

  In the following description, a 3-vector may be a tuple, list or
  numeric array with three elements.

  This function will set up a straight dislocation through a
  specified point with a given Burgers vector.  The first argument
  ('origin', a 3-vector) specifies a point on the dislocation line,
  and the second argument ('line', a 3-vector) gives the dislocation
  line (only the direction of the vector matters). The third
  parameter ('Burgers', a 3-vector) specifies the Burgers vector of
  the dislocation.  Finally, the fourth argument is an optional
  parameter which specifies Poisson's ratio of the material.  The
  default value is 1/3.

  Returns a `setup.displacementfield.DisplacementField` object.
  Displacement fields may be added together (and multiplied by
  scalars).  The dislplacement field can be applied to any 
  Atoms object using field.`apply_to(atoms)`.

``ScrewDislocation(origin, line, b, cut=None, debug=None)``:
  Set up a straight screw dislocation.

  Set up a straight screw dislocation through a specified point with
  a given burgers vector.  The first argument ('origin', a 3-tuple)
  specifies a point on the dislocation line, the second ('line', a
  3-tuple) gives the dislocation line (only the direction of the
  vector matters).  The third ('b', a scalar) gives the Burgers
  vector (positive means parallel to the dislocation line, negative
  means antiparallel).  The third optional argument (cut, a 3-tuple)
  gives a direction that lies in the cutting plane when starting in
  the origin.  If not given, (1.0, 0.0, 0.0) is used unless parallel
  to the dislocation line, in which case (0.0, 1.0, 0.0) is used.

``EdgeDislocation(origin, line, Burgers, PoissonRatio=None, debug=None)``:
  Sets up a straight edge dislocation.

  Sets up a straight edge dislocation through a specified point with
  a given Burgers vector.  The first argument ('origin', a 3-tuple)
  specifies a point on the dislocation line, and the second argument
  ('line', a 3-tuple) gives the dislocation line (only the direction of the
  vector matters). The third parameter
  ('Burgers', a 3-tuple) specifies the Burgers vector of the dislocation.
  The Burgers vector must be perpendicular to the line vector. Finally, the
  fourth argument is an optional parameter which specifies Poisson's ratio
  (unlike in the case of a screw dislocation, this number is needed in order
  to calculate the displacement field). The default value of Poisson's ratio
  is 1/3. 

The DisplacementField object implements a vector function of
position.  DisplacementField can be added, subtracted, multiplied and
divided as expected.  In addition, it has a single method for applying
the field to an atomic-scale system:

  ``field.apply_to(atoms):`` 
        Apply the displacement field to an Atoms object.

        The atoms are modified to reflect the displacement field.

        Two optional arguments are possible.

        fixedpox (default: 0): Adjust the displacement field to keep
        the corners of the computational box fixed by adding a smooth
        displacement field to the specified field.

        usepositions (default: None): If given, it must be an array
        containing the positions of all atoms, to be used
        instead of the positions extracted from the list of atoms.  It
        can be useful if several displacement fields are to be applied
        to the same system, by giving the original positions as this
        argument the fields can be applied sequentially while
        obtaining an effect similar to adding the fields and then
        applying the sum.  This is mainly useful if the displacement
        field contains discontinuities, where a previously applied
        field (or dynamics) may cause some atoms to cross the
        discontinuity.


  
