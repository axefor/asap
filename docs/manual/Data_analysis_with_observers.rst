.. _Data analysis with observers:

============================
Data analysis with observers
============================

Since Asap simulations tend to be very large, it is often advantageous
to analyze the simulation while it is running. This is particularly
the case for parallel simulations, where memory constraints may make
it difficult to load the simulation later.  In such cases, observers
can be very useful.

An observer is an object which is informed about changes to another
object, and then "observes" it.  In its usual form, two objects are
observed, the "subject" and the "observer".  The subject informs the
observer when it has been modified, the observer then analyzes data it
reads form the subject.  

In ASE/Asap, this is altered slightly, and
three objects are involved.  The Atoms object (the subject), a
Dynamics object, and an observer.  The Dynamics object modifies the
Atoms, and notifies the observer that the atoms have been modified.
The observer then analyzes data it gets form the atoms.  This is done
because a Dynamics sometimes modifies the Atoms object more than once
during a "time step", and the Atoms cannot know this.  The resulting
pattern is illustrated in the figure below.

.. figure:: ../_images/observer.png

  The observer pattern as used in ASE/Asap.  When the dynamics informs
  the observer that it has modified the atoms, the observer can do
  whatever analysis of the atoms it was written to do.
