.. _Potentials:

==========
Potentials
==========

Asap supports a number of potentials (Calculators), the most important
being the Effective Medium Theory (:ref:`EMT`).  For each simulation, a
potential must be chosen to describe the atomic interactions.  Below
the potentials are briefly described.

Effective Medium Theory (EMT)
=============================

The :ref:`EMT` potential is the main potential in Asap.  It describes the
late transition metals Ni, Cu, Pd, Ag, Pt and Au that all crystallize
in the FCC structure.  It also describes their alloys.
Experimental support is available for Mg, Zr and Ru.

A special version exists which has been optimized for :ref:`Monte Carlo simulations`.


The Molybdenum potential
========================

The :ref:`Molybdenum` potential describes the BCC metal Molybdenum (Mo).  The potential is mainly made
to describe dislocations in Mo.

**The Molybdenum potential is currently not available in Asap version 3.**  It can quickly be restored if anybody want to use it.


The Lennard-Jones potential
===========================

The good old :ref:`Lennard-Jones` potential is available from Asap version
2.17.7.  Although faster than EMT, it gives a much less accurate
description of the atomic interactions.

The Brenner potential
=====================

The :ref:`Brenner` potential for Carbon, hydrocarbons, Silicon and Germanium. 


OpenKIM models
==============

Models published by the OpenKIM.org_ project are supported by Asap.
This currently means that approximately 150 OpenKIM models can be run
by Asap.  Parallel simulations are supported.  

For more information, see the page on :ref:`OpenKIM support`.

.. _OpenKIM.org: http://openkim.org



.. toctree::
    :hidden:

    potentials/EMT
    potentials/Lennard-Jones
    potentials/Brenner
    potentials/OpenKIM_support
    potentials/Molybdenum

