.. _License:

===============================
 License: ASAP is Free Software
===============================

The newest versions of Asap (version 3.4 and later) are released under the `GNU Lesser General Public License`_ (LGPL) version 3, which is an amendment to the `GNU General Public License`_ (GPL) permitting linking ASAP to other programs not under the GPL.  

The principles of the GPL / LGPL are the following:

* **NO WARRANTY:** You get the program for free, do not sue me if it fries your computer or (more likely) gives wrong results.

* You may use the program for whatever purpose you like.

* You may redistribute the program, modify it, study it, use parts of it (or the whole) in your own programs etc etc; provided that you do not deny others the right to do the same.  So if you distribute derived work, you must do so under the LGPL or the (stricter) GPL.

* You may link ASAP to commercial software, provided you make it clear to the user that ASAP is Free Software under the LGPL, and you give them a reasonable way of getting ASAP's source code.  If you need to modify ASAP to do so, you must release the source code of the modifications (possibly by contributing them to the project).


Full text of the license
========================

The GPL and the LGPL are included in the source code as the files LICENSE and LICENSE.LESSER.  The file LICENSE is the GPL and contains the license proper.  The LGPL (in LICENSE.LESSER) modifies this slightly, but cannot stand alone.

You can also get the text from the `Free Software Foundation`_ (FSF):

* The `GNU General Public License`_.
 
* The `GNU Lesser General Public License`_.


Older versions of Asap
======================

Version 2.X of Asap was released under GPL version 2.

Version 3.0 to 3.3.9 of Asap was released under GPL version 3.


Newer versions of the LGPL
==========================

I have *not* followed the advise of the FSF to release ASAP under LGPL "version 3 or any newer version", as I do not like the idea of releasing software under a yet unknown license.  Instead I invoke section 14 of the GPL, and empower myself (`Jakob Schiøtz`_) or the head of department of the Department of Physics, Technical University of Denmark, to publicly announce that any newer versions may be used also for older versions of Asap.

Future changes
==============

We reserve the right to change the license back to GPL at any time in the future (in fact, the LGPL explicitly gives us this right).  This could happen if it is necessary for Asap's development to include GPL code from other projects.  However, we do not expect to do so.  In such cases, any versions released under LGPL will of course remain LGPL (the license guarantees this), and it will be possible to fork the project and maintain an LGPL version (the license also guarantees this).


.. _Free Software Foundation: http://www.fsf.org/

.. _GNU General Public License: http://www.gnu.org/licenses/gpl.html

.. _GPL: http://www.gnu.org/licenses/gpl.html

.. _GNU Lesser General Public License: http://www.gnu.org/licenses/lgpl.html

.. _LGPL: http://www.gnu.org/licenses/lgpl.html

.. _Jakob Schiøtz: https://www.dtu.dk/person/jakob-schioetz?id=2737
