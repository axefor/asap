.. _Running OpenKIM containers:

==========================
Running OpenKIM containers
==========================

*This is mainly a "note to self" and is only useful for the developer.*

The OpenKIM containers can be useful to test for bugs such as memory leaks in the OpenKIM models.

The docker containers require that I have been added to the relevant DockerHUB project.  Also, Docker Desktop must be running (on a Mac).

Running the docker container the first time::

  docker pull dskarls/openkim-dev:beta.latest.no-validator
  docker run -it --name jakob-kim-dev dskarls/openkim-dev:beta.latest.no-validator bash

This gives a ``bash`` shell in the container environment, where OpenKIM tests and models can be installed.  The surrounding world can be reached with ``ssh``,  but ``git`` is not available.  Exit the container as any other shell.

To restart the container later, use the command::

  docker start -i jakob-kim-dev
