.. _Using AddressSanitizer:

=======================
Using AddressSanitizer
=======================

AddressSanitizer can help you find memory errors and memory leaks in
Asap (and other programs).  Unfortunately, since Asap is a Python
module, you need to rebuild Python with AddressSanitizer.


Requirements
============

This should work on a 64 bit Linux installation, using gcc version 4.9
or later (including gcc version 5.X).


Installing Python with AddressSanitizer
=======================================

Create a folder where the special Python will live.  I made one called
``pysanitize``.  Create a subfolder called ``src``.  Grab the source
code for newest Python 3.X from python.org and place it in the
subfolder.  Then set the following environment variables::

    export CC='gcc -fsanitize=address'
    export CXX='g++ -fsanitize=address'
    export LSAN_OPTIONS=exitcode=0

The first two enables AddressSanitizer in a way that does not get
stripped away by the Python build process.  The last one is because
some of the helper programs compiled during the Python build process
have memory leaks.  The option prevents that these leaks cause the
``make`` program to abort.

Now configure Python and compile it.  The configuration script needs
to be told where you place the new Python.  You also need to disable
Python's own memory allocation magic - that should not be necessary,
but at least with gcc 5.4.0 Python cannot build with the sanitizer
unless this is done.  Finally, I was missing some IPv6 library, and
had to disable that functionality.

::

    cd ~/pysanitize/src/Python-3.5.2
    ./configure  --prefix=$HOME/pysanitize --without-pymalloc --disable-ipv6
    make
    make install

Make sure that you get this new Python first in your PATH, by adding
this line to ``.bashrc`` and then opening a new terminal::

    export PATH=${HOME}/pysanitize/bin:${PATH}

Start python on the command version, check you get the expected
version (compiled today).

NumPy and prerequisites
-----------------------

Now you need to install NumPy.  You need to download the latest
version of setuptools, Cython and numpy by downloading the source code
of the three packages, and install them in that order using the
command::

    python3 setup.py install --prefix=$HOME/pysanitize

Compile Asap with the sanitizer
===============================

Find which makefile defines how you compile::

    $ make version
    Getting configuration from makefile-Linux-x86_64-gnu
    ASAP version 3.9.3

Copy the makefile (in this case makefile-Linux-x86_64-gnu) and call it
makefile-local .  Edit it, and change the two lines defining the
compiler so they look like

::

    CC=gcc -fsanitize=address
    CXX=g++ -fsanitize=address

Then compile Asap as usual.  Since you have changed the compiler
flags, you need to clean everything away first::

    make cleanall
    make depend
    make all

Congratulations, you can now run the tests (with the new Python
compiler, of course).  AddressSanitizer will interrupt the process if
an illegal memory access is performed, and it will report leaks at the
end.  Expect a few leaks from Python itself, but none should originate
in Asap.
