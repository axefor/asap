.. _Installing Asap with a Makefile:

===============================
Installing Asap with a Makefile
===============================

.. contents::


Why install with a Makefile?
============================
   
The recommended way to install Asap is with ``pip``, as described in
the main :ref:`Installation` guide.  However, Asap can also be
installed with a Makefile, this method is more cumbersome and error
prone, but may have some advantages.

* When developing the code, only changed C++ files have to be
  recompiled.

* The same source code folder (on a network files system) can be used
  on different computer architectures.

* Complete control over compilers and compiler options can give a bit
  of extra performance.
   

Prerequisites
=============

To use this code, you need some other software.

A C++ compiler. 
  The GNU compiler is fine, but on Intel and AMD CPUs
  you get better performance from Intel's own compiler.

GNU make.  
  The makefile is unlikely to work with any other version
  of make.  GNU make is the default on Linux and other GNU-based
  Unixes.  On other machines it may be installed under the name gmake
  (try gmake --version).  Otherwise get it from www.gnu.org, and
  install it. 

For parallel simulations ONLY: A version of MPI.  
  We use OpenMPI but other versions should in principle work as well.

Python.  
  A reasonably modern version of Python.  Asap should work with Python
  3.8 or newer.

NumPy and SciPy.
  The standard libraries for numerical work in Python

The Atomic Simulation Environment (ASE).
  This is the part of our code which is shared by Asap, GPAW and
  others.  Get it from http://wiki.fysik.dtu.dk/ase and follow the
  instructions there.  


Download Asap
=============

The newest official release is available from the :ref:`Installation` page.
You can also get Asap from GitLab_.

.. _GitLab: https://gitlab.com/asap/asap



Compiling and installing using the Makefile
===========================================

This is the recommended way to install Asap if you want to use the
Intel compiler and/or if you are installing Asap on a cluster.


1) Unpack the ASAP tar file, or check it out from GitLab_.

2) You *may* have to change the makefile to reflect your local system.

   The Asap Makefile system use a number of configuration files, and
   tries to pick the correct one.  They have names like
   ``makefile-x86_64-gnu`` for use on an Intel 64 bit system with GNU
   compilers.

   You can see which version the system picks by typing ``make
   version``::

	[Lubuntu32] Asap$ make version
	Getting configuration from makefile-i686-gnu
	ASAP version 3.8.3

   If you need to modify the configuration, or if the Asap make system
   chose the wrong one, please copy the desired configuration and name
   it ``makefile-local``. That one will overrule other makefile names.
   You can then edit the file to choose the correct compilers and
   compiler options. 

   If you need to modify this behavior (most likely on exotic systems), please
   read the section `Modifying the Makefile`_ below.


3) Create the makefile dependencies by running

   ::

     make depend

   If you do not have MPI installed you will get error messages during
   the second part of this.  This is harmless.

4) Compile the code. 

   To make only the serial version (e.g. if MPI is not installed):, type

   ::

     make serial

   To make both the serial and parallel version, type

   ::

     make all



Accessing the installation (using $PYTHONPATH)
==============================================


In the following I assume you install in a subdirectory of your home
directory ($HOME or ~).  

1) (optional) Unpack the ASE in some directory (or check it out
   from git) In the following I will assume that it is in $HOME/ase

2) Place the ASAP source code in another directory, for example $HOME/Asap

3) Compile Asap as above.

4) Place the ASE and the both the Asap Python code and the
   compiled modue on the PYTHONPATH: 
 
   Set PYTHONPATH to
   $HOME/ase:$HOME/Asap/Python:$HOME/Asap/XXXXXX where
   XXXXXX is the output of ``uname -m``.  If you use the bash shell on
   an Intel Linux machine, you could place this command in your
   .bashrc

   ::
    
     setenv PYTHONPATH $HOME/ase:$HOME/Asap/Python:$HOME/Asap/x86_64


   Of course you should leave out the $HOME/ase: part if you
   skipped point 1 above.

5) If you change the Asap source code (or update from SVN) you can
   just type ``make`` to generate a new executable.



Installing on multiple different machines
=========================================

You can easily install Asap in a heterogeneous environment, e.g. where
your home directory is NFS mounted to machines with different
architectures (the output of ``uname -m``).  The makefile keeps the
object files from different architectures in different subdirectories.
Just remember to compile and install on a machine of each
architecture.


Modifying the makefile
======================

The only modification you should need to make to the makefile is to
add a specification of how Asap should be compiled on your system
(compiler flags etc).  This is done by adding a file defining the
relevant variable (probably by copying one of the existing files and
modifying it).  The main makefile loads these variables from the first
of the following files that it finds

makefile-local
    A file with this name can be used to local customizations if you
    only compile to a single architecture in the installation
    directory.

makefile-*hostname*
    A file named makefile-*hostname* where *hostname* is replaced with
    the machines hostname (the output of ``uname -n``) can be used for
    local customizations if you compile for multiple architectures in
    the same installation directory.  Compiling with multiple setups
    but the same architecture (the output of ``uname -m``) is not
    supported, as the object files will then end in the same
    subdirectories.

makefile-*arch*-*compiler*
    Files named makefile-*arch*-*compiler* are used to specify setups
    for architectures where multiple compilers are supported
    (presently the ``i686`` and ``x86_64`` architectures).  If you add
    new files of this type, please contribute them.
 
makefile-*arch*
    Files named makefile-*arch* specify setups for architectures where
    only a single compiler is available.  *arch* is the output of
    ``uname -m``.  If you add new files of this type, please
    contribute them.

makefile-default
    This configuration is used if none of the above are found.  **Do
    not edit this file**, instead make a new file of the type
    makefile-*arch* .


When making a new configuration file, the easiest is to copy one of
the existing one (e.g. makefile-default) and modify it.  As an
alternative, include an old one in your file.  For example, on the
machine named ``gandalf`` you want to use the default configuration,
but add an option to CFLAGS.  Make a file called makefile-gandalf::

    include makefile-default
    CFLAGS += -march=wizard
    CXXFLAGS = $(CFLAGS)

