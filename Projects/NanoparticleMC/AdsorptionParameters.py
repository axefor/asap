import numpy as np
#Init Parameter dicts:
#Sgas is standard condition diatomic experimental entropy of gas in eV/K
#Eads is np.array of ads energies with [CN=0,CN=1,CN=....,CN=15]
#Viben is np.array of vibration energies(3 modes e.g. [10meV, 22.1meV,24meV]

#Au O from Mikkel Jorgensen's Bachelor Thesis.
param_AuO = {'Sgas':0.002126,
             'Eads':[-0.35,-0.35,-0.35,-0.35,-0.35,-0.29,-0.45,-0.40,0.06,-0.08, 100,100,100,100,100,100],
             'viben':np.array([41.2E-3,43.3E-3,49.8E-3]),
             'dissoc':True}

#Cu H from Mikkel Jorgensen's Bachelor Thesis
param_CuH = {'Sgas':0.001354444194,
             'Eads':np.array([-0.25,-0.25,-0.25,-0.25,-0.25,-0.25,-0.2,-0.2,0.041,-0.107,100,100,100,100,100,100]),
             'viben':np.array([89.1E-3,95.4E-3,120.7E-3]),
             'dissoc':True}

#Au CO Vibrations with PW on Au-fcc(111)
param_AuCO = {'Sgas':0.00204860144,
              'Eads':np.array([-0.85,-0.85,-0.85,-0.85,-0.58,-0.64,-0.55,-0.35,-0.23,-0.12,100,100,100,100,100,100]),
              #'viben':1E-3*np.array([4.3,4.3,13.8,15.1,26.9,253.4]),
              'viben':1E-3*np.array([16.5, 24.7, 28.3, 43.8, 46.4, 255.2]),
              'dissoc':False}

#Now store them in dict:
params = {'AuO':param_AuO,'CuH':param_CuH,'AuCO':param_AuCO}


class adsorptionparameters:
    

    def __init__(self,sys_species):
        self.spec = sys_species
        self.parameters = params[sys_species]
        #now this parameter holds the desired info.
        
    def get(self,param):
        return self.parameters[param]
        
        



