#PBS -l nodes=6:ppn=8:xeon8
#PBS -q verylong
#PBS -N gas_res_n100_c3
#PBS -m ae
"""Resumes a non-finished amc. Usage: asap-qsub resume_amc_gas.py filename T_fictive Nsteps Tgas Pgas outdir.
Where filename is a finished surface monte carlo, T_fictive is the simulation temperature, Nsteps is the monte carlo steps, Tgas is a gas temperature, Pgas is the gas pressure, outdir is the earlier created directory.
"""
from __future__ import print_function
import os
from asap3.nanoparticle_mc.montecarlo import SurfaceMonteCarloData
from ase.cluster.cubic import FaceCenteredCubic
from ase.cluster import data
from asap3.MonteCarlo.Metropolis import Metropolis
from asap3.MonteCarlo.Moves import SurfaceMove
from asap3 import EMT, MonteCarloEMT, MonteCarloAtoms, EMTRasmussenParameters
import numpy as np
from ase.visualize import view
from asap3.nanoparticle_mc.resizecluster import resizecluster
import sys
from asap3.nanoparticle_mc.atommontecarlodata import AtomMonteCarloData
from ase.parallel import world
from asap3.nanoparticle_mc.AdsCalc import adscalc
from ase.cluster.cluster import Cluster #as ase_Cluster
from time import time,sleep
from asap3.nanoparticle_mc.Logger import *

#Check if user asked for help with -h or --help or -doc
for a in sys.argv:
    if a=="-h" or a == "--help" or a == "-doc" or a == "--documentation":
        print(__doc__, file=sys.stderr)
        sys.exit(0)

#Arguments:
filename = sys.argv[1]
temperature = float(sys.argv[2])
nsteps = int(sys.argv[3])
tgas = float(sys.argv[4])  
pgas = float(sys.argv[5])
outdir = sys.argv[6]
species = "AuCO"

#standard constants:
pgas=1E2 #1mbar
tgas = 300 #600 K

# Make sure that we use the Asap Cluster class instead of the ASE one.
assert hasattr(FaceCenteredCubic, "Cluster")
class MonteCarloCluster(MonteCarloAtoms,Cluster):
    pass
FaceCenteredCubic.Cluster = MonteCarloCluster

use_gas = True


def read_and_do_montecarlo(filename,use_gas,outdir):
    d = SurfaceMonteCarloData()
    d.read(filename)
    print("Starting "+str(len(d))+" sims.")
    surfaces = data.fcc.surface_names
    
    #Determine the missing ones:
    missing = []
    for n in range(len(d)):
        file = outdir+"/a%05i.amc.gz" % n
        if not os.path.exists(file):
            missing.append(n)
    print("Missing " +str(len(missing)) + "simulations") 
        
    #for n in range(0,len(d)):
    for n in range(world.rank,len(missing),world.size):
        file = outdir+"/a%05i.amc.gz" % missing[n]
       
        layers = d[missing[n]][1]  # Really d[n]["layers"]
        multiplicity = d.get_multiplicity(missing[n])
        atoms = FaceCenteredCubic(d.atomic_number,surfaces, layers,latticeconstant=d.lattice_constant)
        resizecluster(atoms, d.fitsize)
        do_monte_carlo(atoms,missing[n],outdir,use_gas,multiplicity)
    world.barrier()#Let the cpu's wait until all in same state.



def do_monte_carlo(atoms,iteration,outdir,use_gas,multiplicity):
    tempcalc = MonteCarloEMT(EMTRasmussenParameters())
    relaxcalc = EMT(EMTRasmussenParameters())  # Faster for the relaxation.
    if use_gas==True:
        atoms.set_calculator(adscalc(tempcalc,temperature=tgas,pressure=pgas,species=species,relax_calc=relaxcalc))
    else:
        atoms.set_calculator(tempcalc)
    Esmc = atoms.get_potential_energy()
        
    mc = Metropolis(atoms=atoms,log=None)
    surfmove =SurfaceMove()
    mc.attach_move(surfmove)
    
    outfilename = "a%05i.amc" % iteration
    
    amcd = AtomMonteCarloData(atoms=atoms, surfmove=surfmove,
                  temp=temperature, filename=outfilename,
                  Esmc=Esmc, multiplicity=multiplicity,total_steps = nsteps)
    mc.attach_observer(amcd.accept_move) #Because default event is at acceptmove
    mc.attach_observer(amcd.reject_move, attime='reject')
    amcd.accept_move() #We need to write the first configuration, 
    
    mc.run(nsteps, temp=temperature)
    
    amcd.write(os.path.join(outdir,outfilename))
    

read_and_do_montecarlo(filename,use_gas,outdir)
