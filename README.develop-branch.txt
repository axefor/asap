Changes implemented in develop branch
==========================


MPI Object initialized when first used.

All objects have had their deallocation changed to finalization to comply with modern Python versions.

The type objects have their fields initialized by name instead of error-prone lists of constants.

The MPI module is now C++ like the rest of the code.

