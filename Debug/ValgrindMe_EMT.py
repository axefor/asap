from __future__ import print_function
from asap3 import *
from ase.build import bulk


atoms = bulk('Al').repeat((8,8,8))
atoms.set_calculator(EMT())

dyn = VelocityVerlet(atoms, 5*units.fs)
dyn.run(2)
print(atoms.get_potential_energy())


