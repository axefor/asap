#!/bin/bash -le
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=schiotz@fysik.dtu.dk
#SBATCH --partition=xeon24
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=00:30:00
#SBATCH --job-name=asap-intel-nightly
#SBATCH --dependency=singleton

echo "Job started:" `date`
echo "Working directory:" `pwd`
echo "CPU_ARCH=$CPU_ARCH"

asap/scripts/nightly-test.sh . intel fast

echo "Job ended SUCCESSFULLY:" `date`
