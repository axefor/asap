#!/bin/bash -le
#SBATCH --mail-type=FAIL
#SBATCH --mail-user=schiotz@fysik.dtu.dk
#SBATCH --partition=xeon24
#SBATCH -N 1
#SBATCH -n 2
#SBATCH --time=04:00:00
#SBATCH --job-name=asap-intel-nightly
#SBATCH --dependency=singleton

echo "Job started:" `date`
echo "This is the SLOW (weekly) version."
echo "Working directory:" `pwd`
echo "CPU_ARCH=$CPU_ARCH"

asap/scripts/nightly-test.sh . intel slow

echo "Job ended SUCCESSFULLY:" `date`
