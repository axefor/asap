
#ifndef PARALLELNEIGHBORLISTINTERFACE
#define PARALLELNEIGHBORLISTINTERFACE


PyObject *PyAsap_NewNeighborCellLocator_Parallel(PyObject *noself, PyObject *args,
                                                 PyObject *kwargs);


#endif // ! PARALLELNEIGHBORLISTINTERFACE
