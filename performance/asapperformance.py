"""Measure ASAP performance on a single core."""

import argparse
import platform
import subprocess
import re
import os
import multiprocessing
import time
import json
import numpy as np
from asap3.md.velocitydistribution import MaxwellBoltzmannDistribution, Stationary
from asap3.md.verlet import VelocityVerlet
from asap3.md.langevin import Langevin
from asap3 import EMT
from asap3 import AsapThreads
try:
    from asap3 import MakeParallelAtoms
except ImportError:
    pass  # Will not support MPI if not compiled into code.
from ase.lattice.cubic import FaceCenteredCubic
from ase import units
from ase.parallel import world, parprint, paropen

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('logN', type=int, help='log10 of number of atoms per core.')
    parser.add_argument('--mpi', action='store_true', help='Run with MPI parallelization')
    parser.add_argument('--processes', type=int, default=None,
                            help='Number of parallel processes')
    parser.add_argument('--threads', type=int, default=1,
                            help='Enable multithreading with this number of threads.')
    action = parser.add_mutually_exclusive_group(required=True)
    action.add_argument('--verlet', action='store_true', help='Verlet dynamics single element')
    action.add_argument('--verletalloy', action='store_true', help='Verlet dynamics alloy')
    action.add_argument('--langevin', action='store_true', help='Langevin dynamics at 300K')
    action.add_argument('--langevinhot', action='store_true', help='Langevin dynamics at 2000K')

    args = parser.parse_args()

    # Sanity check - MPI parallelization cannot be combined with some of the other options.
    if args.mpi:
        if args.processes is not None:
            raise ValueError('You cannot specify both --mpi and --processes')
        if args.threads > 1:
            raise ValueError('You cannot specify both --mpi and --threads')
    
    # Make sure output folder exists
    if args.mpi:
        results = 'results-mpi'
    else:
        results = 'results'
    if not os.path.exists(results) and world.rank == 0:
        os.mkdir(results)
    
    natoms = 10**args.logN
    parprint(f'Number of atoms per core: {natoms}')
    # Set a reasonable step number
    nsteps = 10**(7 - args.logN)
    if nsteps > 10000:
        nsteps = 10000
    if nsteps < 100:
        nsteps = 100
    parprint(f'Number of time steps: {nsteps}')
    info = sysinfo()
    parprint(f'CPU model: {info["cpuname"]}')
    parprint(f'Number of cores: {info["ncores"]}')
    if args.mpi:
        info['ntasks'] = world.size
    else:
        info['ntasks'] = info['ncores']
    info['logN'] = args.logN
    info['nthreads'] = args.threads
    if args.threads > 1:
        parprint(f'Multithreading enabled with {args.threads} threads!')
        info['ntasks'] = info['ntasks'] // args.threads
    if args.processes:
        parprint(f'Forcing using {args.processes} tasks!')
        info['ntasks'] = args.processes

    # If MPI is enabled, figure out how big the system should be
    if args.mpi:
        parprint(f'Running MPI parallelization on {info["ntasks"]} cores.')
        mpicores = tuple(bestfactors(info['ntasks']))
        parprint(f'Core configuration for MPI:', mpicores)
    else:
        mpicores = False
        
    if args.verlet:
        timing, stddev = runsimul(verlet, natoms, info['ntasks'], nsteps=nsteps,
                                      threads=args.threads, mpicores=mpicores)
        info['type'] = 'verlet'
    elif args.verletalloy:
        timing, stddev = runsimul(verlet, natoms, info['ntasks'], nsteps=nsteps,
                                      alloy=True, threads=args.threads, mpicores=mpicores)
        info['type'] = 'verletalloy'
    elif args.langevin:
        timing, stddev = runsimul(langevin, natoms, info['ntasks'], nsteps=nsteps,
                                      alloy=True, threads=args.threads, mpicores=mpicores)
        info['type'] = 'langevin'
    elif args.langevinhot:
        timing, stddev = runsimul(langevin, natoms, info['ntasks'], nsteps=nsteps,
                                      T=2000, alloy=True, threads=args.threads,
                                      mpicores=mpicores)
        info['type'] = 'langevinhot'
    else:
        raise RuntimeError('Unknown task!')
    parprint(f'Timing: {timing} us/atom/core')
    info['timing'] = timing
    info['stddev'] = stddev
    # Make a file name from the date
    now = time.time_ns()
    #fname = 'res_' + time.strftime('%Y.%m.%d_%H.%M.%S') + '_' + str(now) + '.json'
    fname = f'{info["type"]}_{info["logN"]}_{info["ncores"]}_{info["ntasks"]}_{info["nthreads"]}_' + str(now)
    try:
        fname += '_' + os.getenv('SLURM_JOB_ID') # Add if in a slurm job
    except:
        pass
    fname += '.json'
    fname = os.path.join(results, fname)
    parprint(f'Writing results to {fname}')
    with paropen(fname, 'wt') as output:
        json.dump(info, output, indent=2, sort_keys=True)
        output.write('\n')

def runsimul(function, natoms, nprocs, T=300, nsteps=1000, alloy=False, threads=1, mpicores=False):
    kwargs = dict(natoms=natoms)
    processes = []
    if threads > 1:
        parprint(f'Enabling multithreading with {threads} threads.')
        AsapThreads(threads)
    if mpicores:
        results = function(natoms, T, nsteps, alloy, np.random.SeedSequence(), mpicores)
        avg = world.max(results)
        stddev = 0.0
    else:
        arguments = [ (natoms, T, nsteps, alloy, seed) for seed in np.random.SeedSequence().spawn(nprocs) ] 
        with multiprocessing.Pool(nprocs) as pool:
            results = pool.starmap(function, arguments)
        assert len(results) == nprocs
        results = np.array(results)
        avg = results.mean()
        stddev = results.std()
    parprint(f'Average: {avg} +/- {stddev}')
    return avg, stddev
        
def verlet(natoms, T, nsteps, alloy, seed, mpicores=False):
    'Run Verlet simulation.'
    rng = np.random.default_rng(seed)
    atoms = makesystem(natoms, alloy, rng, mpicores)
    MaxwellBoltzmannDistribution(atoms, temperature_K=2*T)
    Stationary(atoms)
    atoms.calc = EMT()
    dyn = VelocityVerlet(atoms, timestep=3*units.fs)
    starttime = time.perf_counter()
    dyn.run(nsteps)
    timing = time.perf_counter() - starttime
    usec = timing * 1e6 / nsteps / len(atoms)
    return usec

def langevin(natoms, T, nsteps, alloy, seed, mpicores=False):
    'Run Langevin simulation.'
    rng = np.random.default_rng(seed)
    atoms = makesystem(natoms, alloy, rng, mpicores)
    MaxwellBoltzmannDistribution(atoms, temperature_K=2*T)
    Stationary(atoms)
    atoms.calc = EMT()
    dyn = Langevin(atoms, timestep=3*units.fs, friction=0.03, temperature_K=T)
    starttime = time.perf_counter()
    dyn.run(nsteps)
    timing = time.perf_counter() - starttime
    usec = timing * 1e6 / nsteps / len(atoms)
    return usec

def makesystem(natoms, alloy, rng, mpicores):
    unitcells = natoms // 4
    size = bestfactors(unitcells)
    atoms = FaceCenteredCubic(size=tuple(size), symbol='Ag')
    if alloy:
        gold = rng.random(len(atoms))
        z = atoms.get_atomic_numbers()
        z[gold > 0.5] = 79
        atoms.set_atomic_numbers(z)
    if mpicores:
        # We create a system on each mpi task, then merge them to a big system.
        # No attempt is made to place the atoms on the right mpi task, Asap
        # will have to take care of this when creating the parallel atoms object.
        #
        # Find which block this is, then move atoms there
        x = world.rank % mpicores[0]
        yz = world.rank // mpicores[0]
        y = yz % mpicores[1]
        z = yz // mpicores[1]
        assert(0 <= z < mpicores[2])
        cell = atoms.get_cell()
        # Move atoms
        offset = x * cell[0] + y * cell[1] + z * cell[2]
        cell[0] *= mpicores[0]
        cell[1] *= mpicores[1]
        cell[2] *= mpicores[2]
        atoms.set_cell(cell)
        atoms.positions += offset
        # Connect to a parallel atoms object - will move atoms to correct mpi task.
        distrotime = time.perf_counter()
        atoms = MakeParallelAtoms(atoms, mpicores)
        distrotime = time.perf_counter() - distrotime
        distrotime = world.max(distrotime)
        parprint(f'Distribution time: {distrotime} sec.')    
    return atoms

def bestfactors(n):
    pf = primefactors(n)
    pf.sort()
    pf.reverse()
    factors = np.ones(3, int)
    for f in pf:
        a = np.argmin(factors)
        factors[a] *= f
    return factors

def primefactors(n):
    i = 2
    factors = []
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            factors.append(i)
    if n > 1:
        factors.append(n)
    return factors


def sysinfo():
    'Get information about the computational ressources.'
    info = {}
    info['cpuname'] = get_processor_name()
    info['ncores'] = multiprocessing.cpu_count()
    try:
        affinity = os.sched_getaffinity(0)
        print('*** AFFINITY:', affinity)
        aff_cores = len(affinity)
    except AttributeError:
        pass
    else:
        if aff_cores != info['ncores']:
            parprint(f'Warning: cpu count disagress with affinity ({info["ncores"]} vs {aff_cores}).')
            if aff_cores < info['ncores'] and aff_cores > 1:
                parprint(f'Going with value from sched_getaffinity: {aff_cores}.')
                info['ncores'] = aff_cores
    return info
        

def get_processor_name():
    if platform.system() == "Windows":
        return platform.processor()
    elif platform.system() == "Darwin":
        os.environ['PATH'] = os.environ['PATH'] + os.pathsep + '/usr/sbin'
        command ="sysctl -n machdep.cpu.brand_string"
        return subprocess.check_output(command).strip()
    elif platform.system() == "Linux":
        command = "cat /proc/cpuinfo"
        all_info = subprocess.check_output(command, shell=True).decode().strip()
        for line in all_info.split("\n"):
            if "model name" in line:
                return re.sub( ".*model name.*:", "", line,1).strip()
    return ""

if __name__ == '__main__':
    main()
    
