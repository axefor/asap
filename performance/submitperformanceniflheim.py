import subprocess
import os
import sys

queue = 'xeon56'
cores = 56


template = '''#!/bin/bash -l
#SBATCH --job-name={command}_{cores}
#SBATCH --partition={queue}
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --cpus-per-task={cores}
#SBATCH --time=10:00:00
#SBATCH --output=slurm-%x-%J.out

source "{venv}/bin/activate"

printenv | grep SLURM

for i in 2 3 4 5 6 7; do
    mpiexec --map-by node:PE=$SLURM_CPUS_PER_TASK --report-bindings python asapperformance.py $i --{command}
done


#for i in 1 2 3 4 5 6 8 10 13 20 40; do
#    time python asapperformance.py 6 --{command} --threads=$i
#    time python asapperformance.py 4 --{command} --threads=$i
#done
'''

venv = os.getenv('VIRTUAL_ENV')
sbatch = ["sbatch"]

#cmds = ['verlet', 'verletalloy', 'langevin', 'langevinhot']
cmds = ['verletalloy',]

for command in cmds:
    script = template.format(**locals())
    print("SCRIPT:")
    print(script)
    print()

    sbatchproc = subprocess.Popen(sbatch, stdin=subprocess.PIPE,
                                stdout=subprocess.PIPE, close_fds=True)
    (out, err) = sbatchproc.communicate(script.encode())
    errcode = sbatchproc.wait()
    if errcode:
        print("sbatch failed with error code", str(errcode), file=sys.stderr)
        print("Command line:", sbatch, file=sys.stderr)
        print("Standard error of command:")
        print(err.decode(errors='replace'))
        sys.exit("sbatch failed")
    print(out.decode())
    print()
    

